package RefDiff.Allcommits;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.eclipse.jgit.lib.Repository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import refdiff.core.RefDiff;
import refdiff.core.api.GitService;
import refdiff.core.rm2.model.refactoring.SDRefactoring;
import refdiff.core.util.GitServiceImpl;
import java.net.URL;
import java.net.URLConnection;

public class Refact extends Thread {
	
	     
	
	public static void main(String[] args) throws IOException  {
		   
		  String fileName =  "clojure.csv"; 
		  final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";	        
		  String M_URL = "https://github.com/refdiff-data/clojure";
		  Document document = Jsoup.connect(M_URL).get();
		  
		
		 //  total commits   
		 Element spanclass = document.select("span.text-emphasized").first();
		// int T_commit = Integer.parseInt(spanclass.text()); 
		 System.out.println("Total Commits = " + spanclass.text());
		    		  
		 //  link to commits     
		 Element link = document.select("li.commits").first();
		 Elements e =    link.getElementsByTag("a");
	 	 
		 String ComitUrl = "http://github.com"+e.attr("href");
		 System.out.println("FirstCommit-links= "+ ComitUrl+"\n");

		 
		 // link within the pagination		
		 
		 Document document2 = Jsoup.connect(ComitUrl).userAgent(USER_AGENT).timeout(0).get();
			 
		 Element pagination = document2.select("div.pagination a").get(0);
		 String Url1    =	pagination.attr("href");
		 //System.out.println("pagination-link = " + Url1);
		 Elements firstcommitlinks = document2.select("div.commit-links-group a");
			 for(Element firstcommitlink : firstcommitlinks) { 
				// System.out.println("firstpageCommit-links="+firstcommitlink.attr("href"));
				 String fpcommit =firstcommitlink.attr("href");
					 if(fpcommit.length()>1) {
						 fpcommit=fpcommit.split("commit/")[1];
						 Reff(fpcommit,fileName);
						  
					 }			 
			 }
		 		 
		 int i=0; 
		 		 
	    String e1="null";
	    while(e1.equals("null")) {
		  
		  URLConnection connection = (new URL(Url1)).openConnection();
	     //Thread.sleep(2000); //Delay to comply with rate limiting
		 connection.setRequestProperty("User-Agent", USER_AGENT);		
		 Document document3 = Jsoup.connect(Url1).userAgent(USER_AGENT).timeout(0).get();
		 Elements e2=document3.select("span.disabled");
			 if(e2.isEmpty()) { 	 
			 Element pagination2 = document3.select("div.pagination a").get(1);	
			 String Url2 = pagination2.attr("href");
			 try{Thread.sleep(500);}catch(Exception e11){System.out.println(e11);}
			 System.out.println("pagination-link "+ i + "=" + Url2);
			 	 
			 
				 Document document4 = Jsoup.connect(Url2).userAgent(USER_AGENT).timeout(0).get();
				 
				 Elements commitlinks = document4.select("div.commit-links-group a");
					 for(Element commitlink : commitlinks) { 
					// System.out.println("Commit-links="+commitlink.attr("href"));
					 String commit =commitlink.attr("href");
						  if(commit.length()>1) {
						  commit=commit.split("commit/")[1];				 		 
						   //commit_collection.add(commit);
						  Reff(commit,fileName);
						  }
					 }
			 Url1=Url2;
			 i++; 
			 }else {e1="notnull";}
	  }    
	
	
	

		
	}
	
	
public static void Reff(String c,String fileName ) throws IOException {

    FileWriter fileWriter = new FileWriter(fileName,true);
    BufferedWriter out = new BufferedWriter(fileWriter);
    	
		RefDiff refDiff = new RefDiff();
	    GitService gitService = new GitServiceImpl();
       
	    try (Repository repository = gitService.cloneIfNotExists("C:/tmp/clojure", "https://github.com/refdiff-data/clojure")) {
        List<SDRefactoring> refactorings = refDiff.detectAtCommit(repository ,c);  
      
	         for (SDRefactoring r : refactorings) {
		            System.out.printf("%s\t%s\t%s\n", r.getRefactoringType().getDisplayName(), r.getEntityBefore().key(), r.getEntityAfter().key());	     
		            out.write(r.getRefactoringType().getDisplayName() + "," +String.valueOf(r.getEntityBefore().key()) +"," + String.valueOf(r.getEntityAfter().key())+"\n");         
		        
		     }
       
               out.close();
         
        }catch(Exception ex) {System.out.println(ex);}
}
	




}
